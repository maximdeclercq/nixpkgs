let
  sources = import ./nix/sources.nix {};
  pkgs = import sources.nixpkgs {
    overlays = [
     (import ./pkgs/dwm/overlay.nix)
     (import ./pkgs/dmenu/overlay.nix)
     (import ./pkgs/slstatus/overlay.nix)
     (import ./pkgs/st/overlay.nix)
    ];
  };
in pkgs
