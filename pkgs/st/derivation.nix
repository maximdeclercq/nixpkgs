{ llvmPackages_14, pkg-config, fontconfig, freetype, harfbuzz, ncurses, xorg, fira-code }:

llvmPackages_14.stdenv.mkDerivation {
  name = "st";
  version = "0.8.5";

  src = builtins.fetchGit {
    url = "ssh://git@gitlab.com/maximdeclercq/st.git";
    ref = "master";
    rev = "bb454c95d40afda65e0a107db99134d8201cc8ca";
  };

  nativeBuildInputs = [ pkg-config fontconfig ];
  buildInputs = [ freetype harfbuzz ncurses xorg.libX11 xorg.libXft ];
  propagatedBuildInputs = [ fira-code ];

  preInstall = ''
    export TERMINFO=$out/share/terminfo
  '';

  installFlags = [ "PREFIX=$(out)" ];
}
