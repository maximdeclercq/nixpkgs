{ llvmPackages_14, pkg-config, fontconfig, freetype, xorg }:

llvmPackages_14.stdenv.mkDerivation {
  name = "slstatus";
  version = "0.0.1";

  src = builtins.fetchGit {
    url = "ssh://git@gitlab.com/maximdeclercq/slstatus.git";
    ref = "master";
    rev = "a89614772d01da5f5f361a539ece676543373ec0";
  };

  nativeBuildInputs = [ pkg-config fontconfig freetype];
  buildInputs = [ xorg.libX11 ];

  installFlags = [ "PREFIX=$(out)" ];
}
