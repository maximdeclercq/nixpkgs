let
  sources = import ../nix/sources.nix {};
  pkgs = import sources.nixpkgs {
    overlays = [(import ./overlay.nix)];
  };
in pkgs.dwm
