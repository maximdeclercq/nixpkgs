{ llvmPackages_14, cmake, extra-cmake-modules, expat, xorg, fira-code }:

llvmPackages_14.stdenv.mkDerivation {
  name = "dwm";
  version = "6.3";

  src = builtins.fetchGit {
    url = "ssh://git@gitlab.com/maximdeclercq/mwm.git";
    ref = "main";
    rev = "04e5161cd512ebde5f5582a0772b65d5cab0fccc";
  };

  nativeBuildInputs = [ cmake extra-cmake-modules ];
  buildInputs = [
    expat
    xorg.libX11
    xorg.libXext
    xorg.libXft
    xorg.libXinerama
    xorg.libxcb
  ];
  propagatedBuildInputs = [ fira-code ];
  enableParallelBuilding = true;

  installPhase = ''
    mkdir -p $out/bin
    mv mwm $out/bin/dwm
  '';
}
