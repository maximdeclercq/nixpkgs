{ llvmPackages_14, pkg-config, fontconfig, freetype, xorg, zlib, fira-code }:

llvmPackages_14.stdenv.mkDerivation {
  name = "dmenu";
  version = "5.1";

  src = builtins.fetchGit {
    url = "ssh://git@gitlab.com/maximdeclercq/dmenu.git";
    ref = "master";
    rev = "a769941970022dc9c8019633abb44083334a0df8";
  };

  nativeBuildInputs = [ pkg-config fontconfig freetype];
  buildInputs = [ xorg.libX11 xorg.libXft xorg.libXinerama zlib ];
  propagatedBuildInputs = [ fira-code ];

  installFlags = [ "PREFIX=$(out)" ];

  preConfigure = ''
    sed -i "s@PREFIX = /usr/local@PREFIX = $out@g" config.mk
  '';

  postPatch = ''
    sed -ri -e 's!\<(dmenu|dmenu_path|stest)\>!'"$out/bin"'/&!g' dmenu_run
    sed -ri -e 's!\<stest\>!'"$out/bin"'/&!g' dmenu_path
  '';
}
